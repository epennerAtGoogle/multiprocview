/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.multiprocview;

import android.graphics.SurfaceTexture;
import android.os.Handler;
import android.os.Looper;
import android.view.Surface;
import android.view.SurfaceHolder;

import android.opengl.GLUtils;
import android.util.Log;

import java.lang.Thread;
import java.util.concurrent.Semaphore;

import junit.framework.Assert;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;

import static android.opengl.GLES20.*;

import java.util.Random;

public class GLProducerThread extends GLThread {

    static final String TAG = "GLProducerThread";

    GLFrameRenderer mRenderer;
    Semaphore mFinishedEvent  = new Semaphore(0);
    Semaphore mStartedEvent = new Semaphore(0);
    Semaphore mFrameEvent = new Semaphore(0);
    int mWidth;
    int mHeight;

    GLProducerThread(GLFrameRenderer renderer, SurfaceHolder surfaceHolder, Semaphore semaphore) {
        mRenderer = renderer;
        mFinishedEvent = semaphore;
        mSurfaceHolder = surfaceHolder;
        mWidth = surfaceHolder.getSurfaceFrame().width();
        mHeight = surfaceHolder.getSurfaceFrame().height();
    }

    GLProducerThread(GLFrameRenderer renderer, Surface surface, int width, int height) {
        mRenderer = renderer;
        mSurface = surface;
        mWidth = width;
        mHeight = height;
    }

    public Handler mHandler;
    public Handler getHandler() {
        return mHandler;
    }

    public int doFrameOnThread() {
        class Holder {
            int result = 0;
        }
        final Holder holder = new Holder();
        mHandler.post(new Runnable() {
            public void run() {
                holder.result = mRenderer.renderFrame();
                if (!mEgl.eglSwapBuffers(mEglDisplay, mEglSurface))
                    Log.e(TAG, "eglSwapBuffers failed");
                mFrameEvent.release();
            }
        });
        mFrameEvent.acquireUninterruptibly();
        return holder.result;
    }

    public int doFrameOnThreadAsync() {
        return -1;
    }

    @Override
    public void start() {
        super.start();
        mStartedEvent.acquireUninterruptibly();
    }

    public void quitSafely() {
        mHandler.getLooper().quitSafely();
    }
    
    @Override
    public void run() {
        initGL();
        Looper.prepare();
        mHandler = new Handler();
        mStartedEvent.release();
        mRenderer.init(mWidth, mHeight);

        Looper.loop();

        mRenderer.shutdown();
        mFinishedEvent.release();
        destroyGL();
    }
}
