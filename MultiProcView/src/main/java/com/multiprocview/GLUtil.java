/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.multiprocview;

import android.opengl.GLUtils;
import android.util.Log;
import android.view.Surface;

import java.nio.IntBuffer;
import java.nio.*;
import java.util.Arrays;
import java.util.Random;

import junit.framework.Assert;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;


import static android.opengl.GLES20.*;

public class GLUtil {

    private static Random mRandom = new Random(System.currentTimeMillis());

    public static void setCommonState() {
        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_BLEND);
        glDepthMask(false);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
        glPixelStorei(GL_PACK_ALIGNMENT, 4);
    }

    public static FloatBuffer createUnitQuadVertBuffer() {
        final float[] quadVertData = {
                0.0f, 0.0f,   1.0f, 1.0f,   0.0f, 1.0f, 
                0.0f, 0.0f,   1.0f, 0.0f,   1.0f, 1.0f };
        FloatBuffer buffer = ByteBuffer.allocateDirect(quadVertData.length * 4)
                            .order(ByteOrder.nativeOrder()).asFloatBuffer();
        buffer.put(quadVertData).position(0);
        return buffer;
    }

    public static int createShader(int shaderType, String source) {
        int shader = glCreateShader(shaderType);
        glShaderSource(shader, source);
        glCompileShader(shader);
        int[] compiled = new int[1];
        glGetShaderiv(shader, GL_COMPILE_STATUS, compiled, 0);
        Assert.assertTrue(compiled[0] == GL_TRUE);
        return shader;
    }

    public static int createProgram(String vertexSource, String fragmentSource) {
        int vertexShader = createShader(GL_VERTEX_SHADER, vertexSource);
        int pixelShader = createShader(GL_FRAGMENT_SHADER, fragmentSource);
        int program = glCreateProgram();
        glAttachShader(program, vertexShader);
        glAttachShader(program, pixelShader);
        glLinkProgram(program);
        int[] linkStatus = new int[1];
        glGetProgramiv(program, GL_LINK_STATUS, linkStatus, 0);
        Assert.assertTrue(linkStatus[0] == GL_TRUE);
        return program;
    }
}
