/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.multiprocview;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Rect;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.util.TypedValue;
import android.view.Choreographer;
import android.view.Choreographer.FrameCallback;
import android.view.Display;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.TextureView;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.TextView;


public class MultiProcViewActivity extends Activity {

    private static final String TAG = "MultiProcViewActivity";

    Point mDisplaySize = new Point();

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        // Bind the render service (so we can use mIRenderService)
        Intent intent = new Intent(getApplication(), RenderService.class);
        //intent.putExtra("KEY1", "Value to be used by the service");
        bindService(intent, mRenderServiceConnection, Context.BIND_AUTO_CREATE);

        Display display = getWindowManager().getDefaultDisplay();
        Point displaySize = new Point();
        display.getRealSize(mDisplaySize);

        setContentView(createStackedView(createSyncTextureView()));
        startTextureViewAnimation();

        getActionBar().hide();
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        //getWindow().setStatusBarColor(0xFF33B5E5);
        //getWindow().setNavigationBarColor(0xFFAA66CC);
    }


    @Override
    protected void onDestroy() {
        Log.v(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        Log.v(TAG, "onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.v(TAG, "onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.v(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.v(TAG, "onStop");
        super.onStop();
    }

    void startTextureViewAnimation() {
        Choreographer.getInstance().postFrameCallback(new FrameCallback() {
            public void doFrame(long frameTimeNanos) {
                //Log.v(TAG, "doFrame");
                Choreographer.getInstance().postFrameCallback(this);
                int top = 0;
                boolean shouldDoFrame = mIRenderService != null && mSurface != null;
                if (mSyncSurfaceTexture != null)
                    shouldDoFrame &= mSyncSurfaceTexture.isReadyForFrames();
                if (shouldDoFrame) {
                    // Send a request to render a frame, then wait for the frame
                    // to arrive, and forward this notification to the TextureView.
                    top = mIRenderService.onDoFrame();
                    if (mSyncSurfaceTexture != null) {
                        mSyncSurfaceTexture.waitOnAndNotifyFrameAvailable();
                    }
                }
                mStackedView.setTranslationY(top + 1);
            }
        });
    }

    TextureView mSyncTextureView;
    SynchronizedSurfaceTexture mSyncSurfaceTexture;
    TextureView createSyncTextureView() {
        mSyncSurfaceTexture = SynchronizedSurfaceTexture.Create();
        mSyncTextureView = new TextureView(getApplication());
        mSyncTextureView.setSurfaceTextureListener(mTextureViewListener);
        mSyncTextureView.setSurfaceTexture(mSyncSurfaceTexture);
        return mSyncTextureView;
    }

    TextureView createTextureView() {
        TextureView textureView = new TextureView(getApplication());
        textureView.setSurfaceTextureListener(mTextureViewListener);
        return textureView;
    }

    SurfaceView createSurfaceView() {
        SurfaceView surfaceView = new SurfaceView(getApplication());
        surfaceView.setZOrderOnTop(true);
        surfaceView.getHolder().addCallback(mSurfaceViewListener);
        return surfaceView;
    }

    BasicGLSurfaceView createBasicGLSurfaceView() {
        BasicGLSurfaceView basicGLSurfaceView = new BasicGLSurfaceView(getApplication());
        return basicGLSurfaceView;
    }

    TextView mStackedView;
    FrameLayout createStackedView(View view) {
        FrameLayout frameLayout = new FrameLayout(this);
        TextView textView = new TextView(this);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
        textView.setText("Hello Synchronized World");
        textView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        textView.setBackgroundColor(0xFFFFFFFF);
        textView.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
                                                              LayoutParams.WRAP_CONTENT,
                                                              Gravity.TOP));
        mStackedView = textView;
        frameLayout.addView(view);
        frameLayout.addView(textView);
        return frameLayout;
    }

    ScrollView createScrollView(View view) {
        view.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT,
                                                          mDisplaySize.y / 2,
                                                          Gravity.CENTER));
        TextView textView = new TextView(this);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
        String longText = "";
        for (int i = 0; i < 20; i++) longText += mLoremIpsum;
        textView.setText(longText);

        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.addView(view);
        linearLayout.addView(textView);

        ScrollView scrollView = new ScrollView(this);
        scrollView.addView(linearLayout);
        return scrollView;
    }

    // Track the state of the service surface.
    boolean mHaveSurface = false;
    boolean mServiceHasSurface = false;
    Surface mSurface = null;
    Rect mSurfaceRect = null;
    
    void updateServiceSurface() {
        if (mIRenderService == null || mHaveSurface == mServiceHasSurface)
            return;
        if (mHaveSurface) {
            mIRenderService.onSurfaceAvailable(mSurface,
                                               mSurfaceRect.width(),
                                               mSurfaceRect.height());
            
        } else {
            mIRenderService.onSurfaceDestroyed();
        }
        mServiceHasSurface = mHaveSurface;
    }

    // Connection to the render service.
    private EasyRenderService mIRenderService;
    private ServiceConnection mRenderServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder binder) {
            Log.v(TAG, "onServiceConnected");
            IRenderService service = IRenderService.Stub.asInterface(binder);
            mIRenderService = new EasyRenderService(service);
            updateServiceSurface();
            mIRenderService.testInterface(42);
        }
        public void onServiceDisconnected(ComponentName className) {
            Log.v(TAG, "onServiceDisconnected");
            mIRenderService = null;
            // The service crashed. Create a new one!
            Intent intent = new Intent(getApplication(), RenderService.class);
            bindService(intent, mRenderServiceConnection, Context.BIND_AUTO_CREATE);
        }
    };

    // SurfaceView callbacks if we have one.
    private SurfaceHolder.Callback2 mSurfaceViewListener = new SurfaceHolder.Callback2() {
        public void surfaceCreated(SurfaceHolder holder) {
            mSurface = holder.getSurface();
            mSurfaceRect = holder.getSurfaceFrame();
            Log.v(TAG, "surfaceCreated " + mSurfaceRect.width() + " " + mSurfaceRect.height());
            mHaveSurface = true;
            updateServiceSurface();
        }
        public void surfaceDestroyed(SurfaceHolder holder) {
            Log.v(TAG, "surfaceDestroyed");
            mSurface = null;
            mHaveSurface = false;
            updateServiceSurface();
        }
        public void surfaceRedrawNeeded(SurfaceHolder holder) {
            Log.v(TAG, "surfaceRedrawNeeded");
            mIRenderService.onDoFrame();
        }
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Log.v(TAG, "surfaceChanged");
            mIRenderService.onSurfaceChanged(width, height);
        }
    };

    // TextureView callbacks if we have one.
    private TextureView.SurfaceTextureListener mTextureViewListener = new TextureView.SurfaceTextureListener() {
        public void	onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            Log.v(TAG, "onSurfaceTextureAvailable");
            mSurface = new Surface(surface);
            mSurfaceRect = new Rect(0,0,width,height);
            mHaveSurface = true;
            updateServiceSurface();
        }
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            Log.v(TAG, "onSurfaceTextureDestroyed");
            mSurface = null;
            mHaveSurface = false;
            updateServiceSurface();
            return true; // true to release the surface.
        }
        public void	onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            Log.v(TAG, "onSurfaceTextureSizeChanged");
            if (!mHaveSurface)
                onSurfaceTextureAvailable(surface, width, height);
        }
        public void	onSurfaceTextureUpdated(SurfaceTexture surface) {
            Log.v(TAG, "onSurfaceTextureUpdated");
        }
    };

    private String mLoremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris suscipit leo et pretium vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam varius nisi ac diam ornare, eget fermentum massa congue. Nam tortor lacus, placerat a cursus eu, viverra ac nisi. Nunc rutrum feugiat aliquet. Mauris ullamcorper, mauris eget pretium rutrum, massa est varius metus, eget ultricies dolor mauris venenatis nulla. Sed sollicitudin pretium leo in porttitor. Proin accumsan condimentum dictum. Aliquam ac ullamcorper ligula. Nunc aliquam, leo et semper sagittis, diam nunc iaculis nulla, in varius massa ipsum vitae lacus. Pellentesque non elit in mi sodales ullamcorper.";

    // Catch all the service exceptions and log them for now
    private class EasyRenderService implements IRenderService {
        IRenderService mIService;
        public EasyRenderService(IRenderService IService) {
            mIService = IService;
        }
        public IBinder asBinder() {
            return mIService.asBinder();
        }
        private void LogE(RemoteException r) {
            Log.v(TAG, "RemoteException");
            throw new RuntimeException(r);
        }
        public void testInterface(int anInt) {
            try { mIService.testInterface(anInt); }
            catch (RemoteException r) { LogE(r); }
        }
        public void onSurfaceAvailable(Surface surface, int width, int height) {
            try { mIService.onSurfaceAvailable(surface, width, height); }
            catch (RemoteException r) { LogE(r); }
        }
        public void onSurfaceDestroyed() {
            try { mIService.onSurfaceDestroyed(); }
            catch (RemoteException r) { LogE(r); }
        }
        public void onSurfaceChanged(int width, int height) {
            try { mIService.onSurfaceChanged(width, height); }
            catch (RemoteException r) { LogE(r); }
        }
        public int onDoFrame() {
            int result = 0;
            try { result = mIService.onDoFrame(); }
            catch (RemoteException r) { LogE(r); }
            return result;
        }
    }
}