/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.multiprocview;

import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import java.util.concurrent.Semaphore;


public class SynchronizedSurfaceTexture extends SurfaceTexture {
    static final String TAG = "SynchronizedSurfaceTexture";

    /*
     * Create a SynchronizedSurfaceTexture. For legacy reasons, we must do this on a different
     * thread in order to set the onFrameAvailable handler. The returned SurfaceTexture is already
     * detached from GL.
     */
    public static SynchronizedSurfaceTexture Create() {
        initHandler();
        class Holder {
            SynchronizedSurfaceTexture mValue;
        }
        final Holder holder = new Holder();
        final Semaphore created = new Semaphore(0);
        mHandler.post(new Runnable() {
            public void run() {
                holder.mValue = new SynchronizedSurfaceTexture();
                created.release();
            }
        });
        created.acquireUninterruptibly();
        return holder.mValue;
    }

    /*
     * Create a plain SurfaceTexture on another thread. This is to test whether
     * we we need to inherit from SurfaceTexture, or whether creating it off thread
     * is sufficient (to get the threaded onFrameAvailable).
     */
    public static SurfaceTexture CreateOffThreadTest() {
        initHandler();
        class Holder {
            SurfaceTexture mValue;
        }
        final Holder holder = new Holder();
        final Semaphore created = new Semaphore(0);
        mHandler.post(new Runnable() {
            public void run() {
                holder.mValue = new SurfaceTexture(0);
                holder.mValue.detachFromGLContext();
                created.release();
            }
        });
        created.acquireUninterruptibly();
        return holder.mValue;
    }
    
    private SynchronizedSurfaceTexture() {
        // Initialize in a detach state.
        super(0);
        super.detachFromGLContext();
        mOverrideFrameAvailable = false;
        super.setOnFrameAvailableListener(mListener);
        mOverrideFrameAvailable = true;
    }

    public boolean isReadyForFrames() {
        return mClientListener != null;
    }

    boolean mIsAttached;
    public void attachToGLContext(int texName) {
        Log.v(TAG, "detachFromGLContext");
        super.attachToGLContext(texName);
        mIsAttached = true;
    }

    public void detachFromGLContext(int texName) {
        Log.v(TAG, "detachFromGLContext");
        super.detachFromGLContext();
        mIsAttached = false;
    }

    public void release() {
        super.release();
        Log.v(TAG, "release");
    }

    // Note, we wish to override both setOnFrameAvailableListener calls. However, the base
    // implementation could call one from the other. To handle this, whenever we call into
    // super, we disable overriding of these methods.
    boolean mOverrideFrameAvailable = false;
    public void setOnFrameAvailableListener(OnFrameAvailableListener listener, Handler handler) {
        Log.v(TAG, "setOnFrameAvailableListener");
        assert Looper.myLooper() == Looper.getMainLooper();
        if (handler != null) assert handler.getLooper() == Looper.getMainLooper();
        if (!mOverrideFrameAvailable) {
            super.setOnFrameAvailableListener(listener, handler);
            return;
        }
        mOverrideFrameAvailable = false;
        if (listener != null) super.setOnFrameAvailableListener(mListener, mHandler);
        else                  super.setOnFrameAvailableListener(null, mHandler);
        mOverrideFrameAvailable = true;
        mClientListener = listener;
    }

    public void setOnFrameAvailableListener(OnFrameAvailableListener listener) {
        Log.v(TAG, "setOnFrameAvailableListener");
        assert Looper.myLooper() == Looper.getMainLooper();
        if (!mOverrideFrameAvailable) {
            super.setOnFrameAvailableListener(listener);
            return;
        }
        mOverrideFrameAvailable = false;
        if (listener != null) super.setOnFrameAvailableListener(mListener);
        else                  super.setOnFrameAvailableListener(null);
        mOverrideFrameAvailable = true;
        mClientListener = listener;
    }

    Semaphore mFrameAvailable = new Semaphore(0);
    OnFrameAvailableListener mClientListener;
    OnFrameAvailableListener mListener = new OnFrameAvailableListener() {
        public void onFrameAvailable(SurfaceTexture surfaceTexture) {
            assert Looper.myLooper() == mHandler.getLooper();
            Log.e(TAG, "onFrameAvailable");
            mFrameAvailable.release();
        }
    };

    public void waitOnAndNotifyFrameAvailable() {
        assert Looper.myLooper() == Looper.getMainLooper();
        mFrameAvailable.acquireUninterruptibly();
        if (mClientListener != null)
            mClientListener.onFrameAvailable(this);
    }

    public void notifyFrameAvailable() {
        if (mClientListener != null)
            mClientListener.onFrameAvailable(this);
    }

    static volatile Handler mHandler;
    static void initHandler() {
        if (mHandler == null) {
            final Semaphore started = new Semaphore(0);
            Thread thread = new Thread() {
                public void run() {
                    Looper.prepare();
                    mHandler = new Handler();
                    started.release();
                    Looper.loop();
                }
            };
            thread.start();
            started.acquireUninterruptibly();
        }
    }
}