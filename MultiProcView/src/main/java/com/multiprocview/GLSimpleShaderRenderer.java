/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.multiprocview;

import static android.opengl.GLES20.*;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.egl.EGLDisplay;

import junit.framework.Assert;
import java.nio.*;
import java.util.Arrays;
import java.util.concurrent.locks.LockSupport;
import android.graphics.SurfaceTexture;
import android.opengl.GLUtils;
import android.util.Log;
import android.view.Surface;

public class GLSimpleShaderRenderer implements GLFrameRenderer{

    static final String TAG = "GLSimpleShaderRenderer";

    private int mScreenWidth;
    private int mScreenHeight;

    private long mStartTimeInMillis;
    private long mFrameTimeInMillis;
    
    private FloatBuffer mQuadVertBuffer;
    private Program mProgram;
    
    GLSimpleShaderRenderer() {
    }

    @Override
    public void init(int width, int height) {
        mScreenWidth = width;
        mScreenHeight = height;
        mQuadVertBuffer = GLUtil.createUnitQuadVertBuffer();
        mProgram = new Program(mScreenSpaceVertexShader, mFractalFragmentShader);
        mStartTimeInMillis = System.currentTimeMillis();
        GLCHK();
    }

    @Override
    public void shutdown() {
    }

    private void drawScreenQuad(Program program, int texture, int x, int y, int width, int height) {
        glUseProgram(program.mProgram);

        glVertexAttribPointer(program.mCoordHandle, 2, GL_FLOAT, false, 8, mQuadVertBuffer);
        glEnableVertexAttribArray(program.mCoordHandle);
        glUniform1i(program.mTextureHandle, 0);
        glUniform2f(program.mScreenSizeHandle, mScreenWidth, mScreenHeight);
        glUniform2f(program.mTileSizeHandle, width, height);
        glUniform2f(program.mOffsetHandle, x, y);
        glUniform1f(program.mTimeHandle, mFrameTimeInMillis / 1000.f);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(program.mTextureTarget, texture);
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }

    // Animated clear for debugging.
    public void renderAnimatedClear() {
        float value = mFrameTimeInMillis % 255 / 255.f;
        value = (float)Math.pow(value, 2.2);
        if (mFrameTimeInMillis / 255 % 2 == 1)
            value = 1.f - value;
        long color = mFrameTimeInMillis / 255 / 2 % 3;
        float r = (color == 0) ? value : 0;
        float g = (color == 1) ? value : 0;
        float b = (color == 2) ? value : 0;
        glClearColor(r, g, b, 1.f);
        glClear(GL_COLOR_BUFFER_BIT);
    }
    
    public int renderFrame() {
        mFrameTimeInMillis = System.currentTimeMillis() - mStartTimeInMillis;

        // Clear to white
        glEnable(GL_SCISSOR_TEST);
        glScissor(0, 0, mScreenWidth, mScreenHeight);
        glViewport(0, 0, mScreenWidth, mScreenHeight);
        glClearColor(1.f, 1.f, 1.f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT);

        // Animate top of clear
        int height = (int)((float)mScreenHeight * (Math.sin(mFrameTimeInMillis / 500.f) / 2.f + 0.5f));
        Log.e(TAG, "height " + height);
        glScissor(0, 0, mScreenWidth, height);
        glViewport(0, 0, mScreenWidth, height);
        glClearColor(0.f, 0.f, 0.f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT);
        return mScreenHeight - height;

        //try {Thread.sleep(2 * 1000);} catch(Exception e) {}

        //int tileSize = 64;
        //for (int y = 0; y < mScreenHeight; y += tileSize) {
        //    for (int x = 0; x < mScreenWidth; x += tileSize) {
        //        drawScreenQuad(mProgram, 0, x, y, tileSize, tileSize);
        //    }
        //}

        //glFinish();
    }

    void GLCHK() {
        int error;
        while ((error = glGetError()) != GL_NO_ERROR) {
            Log.e(TAG, "glError " + error);
            throw new RuntimeException("glError " + error);
        }
    }
    
    private final String mScreenSpaceVertexShader =
        "uniform vec2 screenSize;\n" +
        "uniform vec2 tileSize;\n" +
        "uniform vec2 offset;\n" +
        "attribute vec2 coord;\n" +
        "varying vec2 vTexCoord;\n" +
        "void main() {\n" +
        "  vec2 screenCoord = (coord * tileSize + offset) / screenSize; \n" +
        "  vec2 ndcCoord = screenCoord * 2.0 - 1.0; \n" +
        "  gl_Position = vec4(ndcCoord, 0.0, 1.0);\n" +
        "  vTexCoord = ndcCoord * 1.5;\n" +
        "}\n";
    
    private final String mVertexShader =
        "uniform vec2 screenSize;\n" +
        "uniform vec2 tileSize;\n" +
        "uniform vec2 offset;\n" +
        "attribute vec2 coord;\n" +
        "varying vec2 vTexCoord;\n" +
        "void main() {\n" +
        "  vec2 screenCoord = (coord * tileSize + offset) / screenSize; \n" +
        "  vec2 ndcCoord = screenCoord * 2.0 - 1.0; \n" +
        "  gl_Position = vec4(ndcCoord, 0.0, 1.0);\n" +
        "  vTexCoord = coord;\n" +
        "}\n";

    private final String mTextureFragmentShader =
        "precision mediump float;\n" +
        "varying vec2 vTexCoord;\n" +
        "uniform sampler2D texture;\n" +
        "uniform vec4 test; \n" +
        "void main() {\n" +
        "  gl_FragColor = texture2D(texture, vTexCoord); \n" +
        "}\n";

    private final String mFractalFragmentShader =
        "precision highp float;\n" +
        "varying vec2 vTexCoord;\n" +
        "uniform float time;\n" +
        "void main() {\n" +
        "    vec2 cc = vec2( cos(.25*time), sin(.25*time*1.423) );\n" +
        "    float dmin = 1000.0;\n" +
        "    vec2 z = vTexCoord*vec2(1.33, 1.0);\n" +
        "    for(int i=0; i < 1024; i++ ) {\n" +
        "        z = cc + vec2( z.x*z.x - z.y*z.y, 2.0*z.x*z.y );\n" +
        "        float m2 = dot(z,z);\n" +
        //"        if (m2 > 100.0) break; \n" +
        "        dmin = min(dmin, m2);\n" +
        "    }\n" +
        "    float color = sqrt(sqrt(sqrt(dmin)));\n" +
        "    gl_FragColor = vec4(color, color, color, 1.0);\n" +
        "}\n";
    
    private class Program {
        public Program(String vertexShader, String fragmentShader) {
            mProgram = GLUtil.createProgram(vertexShader, fragmentShader);
            mScreenSizeHandle = glGetUniformLocation(mProgram, "screenSize");
            mTileSizeHandle = glGetUniformLocation(mProgram, "tileSize");
            mOffsetHandle = glGetUniformLocation(mProgram, "offset");
            mTextureHandle = glGetUniformLocation(mProgram, "texture");
            mOffsetTextureHandle = glGetUniformLocation(mProgram, "offsetTexture");
            mTimeHandle = glGetUniformLocation(mProgram, "time");
            mCoordHandle = glGetAttribLocation(mProgram, "coord");
            mTextureTarget = GL_TEXTURE_2D;
        }
        public int mProgram;
        public int mScreenSizeHandle;
        public int mTileSizeHandle;
        public int mOffsetHandle;
        public int mCoordHandle;
        public int mTextureHandle;
        public int mOffsetTextureHandle;
        public int mTimeHandle;
        public int mTextureTarget;
    }
}
