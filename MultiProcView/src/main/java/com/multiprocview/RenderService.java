/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.multiprocview;

import android.app.Service;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.IBinder;
import android.os.Process;
import android.util.Log;
import android.view.Surface;

import android.view.inputmethod.BaseInputConnection;


import static android.opengl.GLES20.*;

public class RenderService extends Service {

    private static final String TAG = "RenderService";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v(TAG, "RenderService onStartCommand");
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.v(TAG, "RenderService onBind");
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    Surface mSurface;
    GLProducerThread mProducerThread;
    private GLFrameRenderer mGLRenderer = new GLSimpleShaderRenderer();

    private final IRenderService.Stub mBinder = new IRenderService.Stub() {
        public void testInterface(int anInt) {
            Log.v(TAG, "testInterface " + anInt);
            BaseInputConnection baseInputConnection = new BaseInputConnection(null, false);
        }
        public void onSurfaceAvailable(Surface surface, int width, int height) {
            Log.v(TAG, "onSurfaceAvailable");
            mSurface = surface;
            assert mProducerThread == null;
            mProducerThread = new GLProducerThread(mGLRenderer, mSurface, width, height);
            mProducerThread.start();
        }
        public void onSurfaceDestroyed() {
            Log.v(TAG, "onSurfaceDestroyed");
            if (mProducerThread != null)
                mProducerThread.quitSafely();
            mSurface.release();
            mProducerThread = null;
            mSurface = null;
            Log.v(TAG, "onSurfaceDestroyed done");
        }
        public void onSurfaceChanged(int width, int height) {
            Log.v(TAG, "onSurfaceChanged " + width + " " + height);
        }
        public int onDoFrame() {
            Log.v(TAG, "onDoFrame");
            if (mSurface == null)
                return -1;
            return mProducerThread.doFrameOnThread();
        }
    };
}
